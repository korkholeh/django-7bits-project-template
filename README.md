# 7bits : Django project template

## Basic requirements

- python >= 2.7
- Django 1.4
- pip
- virtualenv/wrapper (optional)

## How to use the template

    django-admin.py startproject --template=https://bitbucket.org/korkholeh/django-7bits-project-template/get/master.tar.gz --extension py,md,gitignore my_project

    cd my_project
    virtualenv --no-site-packages env_my_project
    source env_my_project/bin/activate

    pip install -r requirements.txt

    ./manage.py syncdb --all
    ./manage.py migrate --fake
    ./manage.py runserver


