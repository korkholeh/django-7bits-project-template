# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from profiles.models import Profile
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as oldUserAdmin

class ProfileInline(admin.StackedInline):
    fieldsets = (
        (_(u'Details'), {
            'fields': (
                'full_name', 
                'homepage', 
                'twitter', 
                'facebook', 
                'vkontakte', 
                'icq',
                'jabber',
                ),
        }),
    )

    model = Profile
    fk_name = 'user'
    can_delete = False
    extra = 0

class UserAdmin(oldUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'email')}),
        (_(u'Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_(u'Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('username', 'full_name_preview', 'email', 'date_joined', 'is_staff', 'is_active')
    search_fields = ['username', 'email']
    list_per_page = 100
    inlines = [ProfileInline]

    def full_name_preview(self, obj):
        return obj.profile.full_name
    full_name_preview.short_description = _(u'Full name')


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

