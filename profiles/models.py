# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User
from django.db.models import signals

class Profile(models.Model):  
    user = models.OneToOneField(User)  
    
    full_name = models.CharField(_(u'Full name'), max_length=70)
    homepage = models.URLField(_(u'Homepage'), verify_exists=False, blank=True)
    twitter = models.URLField(_(u'Twitter'), verify_exists=False, blank=True)
    facebook = models.URLField(_(u'Facebook'), verify_exists=False, blank=True)
    vkontakte = models.URLField(_(u'VK.com'), verify_exists=False, blank=True)
    icq = models.CharField(_(u'ICQ'), max_length=30, blank=True)
    jabber = models.CharField(_(u'Jabber'), max_length=70, blank=True)    

    def __unicode__(self):  
          return "%s's profile" % self.user  

def create_user_profile(sender, instance, created, **kwargs):  
    if created:  
        profile, created = Profile.objects.get_or_create(user=instance)  

signals.post_save.connect(create_user_profile, sender=User) 

